
; You may customize this and other start-up templates; 
; The location of this template is c:\emu8086\inc\0_com_template.txt

org 100h

; add your code here 

.CODE

MOV AH,1H
INT 21H
MOV BL,AL

MOV AH,2H
MOV DL,0DH 
INT 21H
MOV DL,0AH
INT 21H

CMP BL,60H
JG TOUPPER 
JL TOLOWER

TOLOWER:
ADD BL,20H
MOV DL, BL
INT 21H
RET

TOUPPER:
SUB BL,20H
MOV DL,BL
INT 21H
RET

ret




